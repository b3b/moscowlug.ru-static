var next_meet_date = new Date(2015, 07, 29);
var next_meet_date_string = '29 августа';

var geoData = {
    "type": "Feature",
    "geometry": {
        "type": "LineString",
        "coordinates": [
            [
                37.629064321517944,
                55.74228409763208
            ],
            [
                37.62926816940307,
                55.7422025651605
            ],
            [
                37.62987434864044,
                55.74241696499851
            ],
            [
                37.63000309467316,
                55.74354329972739
            ],
            [
                37.63036787509918,
                55.74359161362662
            ],
            [
                37.63071656227112,
                55.74357651553955
            ],
            [
                37.63064682483673,
                55.74389055454801
            ],
            [
                37.63060927391052,
                55.74406569058957
            ]
        ]
    }
};

$(document).ready(function() {
    $("#meet_calendar").datepicker({
        defaultDate: next_meet_date
    });
    $("#next_meet_date").text(next_meet_date_string);

    var map = L.map("map", {"center": [55.74293, 37.62960], "zoom": 17});

    var mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; ' + mapLink + ' Contributors',
        maxZoom: 18,
    }).addTo(map);

    var geoStyle = {
        "color": "#3333ff",
        "weight": 8,
        "opacity": 0.64
    };
    L.geoJson(geoData, { style: geoStyle }).addTo(map);
    L.marker([55.74406569058957, 37.63060927391052]).addTo(map);
});
